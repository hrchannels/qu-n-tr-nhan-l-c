# Quản trị nhân lực

Hầu hết mọi ứng viên định hướng phát triển sự nghiệp trong lĩnh vực nhân sự đều hướng đến vai trò HRM nhưng để hiểu rõ HRM là gì và được kỳ vọng mang đến cho doanh nghiệp thì không phải ứng viên nào cũng nắm rõ.
